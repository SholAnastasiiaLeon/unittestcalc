﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Menu
    {
        public double FirstNumbers()
        {
            Console.WriteLine("Enter first number: ");
            return double.Parse(Console.ReadLine());
        }
        public double SecondNumbers()
        {
            Console.WriteLine("Enter second number: ");
            return double.Parse(Console.ReadLine());
        }
        public string Action()
        {
            Console.WriteLine("Enter the action: '+', '-', '*', '/' , '%'");
            return Console.ReadLine();
        }
    }
}
