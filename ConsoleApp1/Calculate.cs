﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Calculate
    {
        public double Sum(double firsNumber, double secondNumber)
        {
            double sum = firsNumber + secondNumber;
            return sum;
        }
        public double Division(double firsNumber, double secondNumber)
        {
            if (secondNumber == 0)
            {
                throw new DivideByZeroException("Devide by zero is impossible");
            }
            else
            {
                double division = firsNumber / secondNumber;
                return division;
            }
        }
        public double Substraction(double firsNumber, double secondNumber)
        {
            double substraction = firsNumber - secondNumber;
            return substraction;
        }
        public double Multiplication(double firsNumber, double secondNumber)
        {
            double multiplication = firsNumber * secondNumber;
            return multiplication;
        }
        public double DivisionFrom(double firsNumber, double secondNumber)
        {
            double divisionFrom = firsNumber % secondNumber;
            return divisionFrom;
        }
    }
}
