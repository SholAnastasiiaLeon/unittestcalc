﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class Program
    {
        static void Main(string[] args)
        {
            do
            {
                try
                {
                    Menu menu = new Menu();
                    Calculate calculate = new Calculate();
                    double firsNumber = menu.FirstNumbers();
                    double secondNumber = menu.SecondNumbers();
                    string action = menu.Action();
                    double result = 0;
                    switch (action)
                    {
                        case "+":
                            result = calculate.Sum(firsNumber, secondNumber);
                            break;
                        case "-":
                            result = calculate.Substraction(firsNumber, secondNumber);
                            break;
                        case "*":
                            result = calculate.Multiplication(firsNumber, secondNumber);
                            break;
                        case "/":

                            try
                            {
                                result = calculate.Division(firsNumber, secondNumber);
                            }
                            catch (DivideByZeroException exception)
                            {
                                Console.WriteLine(exception.Message);
                            }
                            finally
                            {
                                Console.WriteLine("finally");
                            }

                            break;
                        case "%":
                            result = calculate.DivisionFrom(firsNumber, secondNumber);
                            break;
                        default:
                            Console.WriteLine("Unknown action...");
                            break;
                    }
                    Console.WriteLine(result);
                    Console.ReadLine();
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("You can enter only number");
                }
            }while(true);

        }
    }
}
