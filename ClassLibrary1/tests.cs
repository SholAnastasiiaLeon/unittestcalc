﻿using Calculator;
using NUnit.Framework;
using System.Linq;

namespace Unittests
{
    [TestFixture]
    public class tests
    {
        [TestCase(964, 248, 1212)]
        [TestCase(1, -5, -4)]
        [TestCase(-3, -7500, -7503)]
        [TestCase(-2345671, 0, -2345671)]
        [TestCase(0, 0, 0)]
        [TestCase(1123.87, 897.45,2021.32 )]
       
        public void SumOfTwoNumbers(double firsNumber, double secondNumber, double expectedResult)
        {
            double result = new Calculate().Sum(firsNumber, secondNumber);
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(500,20,25)]
        [TestCase(-80, -4, 20)]
        [TestCase(0,456, 0)]
        [TestCase(100, 2.5, 40)]
        [TestCase(0, 0.5, 0)]
        [TestCase(4.5, 1.5, 3)]

        public void DivisionOfTwoNumbers(double firsNumber, double secondNumber, double expectedResult)
        {
            double result = new Calculate().Division( firsNumber,  secondNumber);
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase(10, 5, 5)]
        [TestCase(10, -5, 15)]
        [TestCase(-67, -1, -66)]
        [TestCase(0, 5, -5)]
        [TestCase(10, 0, 10)]
        [TestCase(10.5, 3.5, 7)]
        [TestCase(50.3, 2, 48.3)]
        [TestCase( 6.7, 0, 6.7)]

        public void SubstractionOfTwNumbers(double firstNumber, double secondNumber, double exResult)
        {
            double result = new Calculate().Substraction(firstNumber, secondNumber);
            Assert.AreEqual(exResult, result);
        }

        [TestCase(1, 5, 5)]
        [TestCase(-11, -5, 55)]
        [TestCase(-2,4, -8)]
        [TestCase(0, 0, 0)]
        [TestCase(7, 0, 0)]
        [TestCase(1.5, 1.5, 2.25)]
        [TestCase(1.7, 0, 0)]
        [TestCase(7.5, 2, 15)]

        public void MultiplicationOfTwNumbers(double firstNumber, double secondNumber, double exResult)
        {
            double result = new Calculate().Multiplication(firstNumber, secondNumber);
            Assert.AreEqual(exResult, result);
        }


        [TestCase(3, 2, 1)]
        [TestCase(6, -4, 2)]
        [TestCase(-16, -4, 0)]
        [TestCase(4.5, 3, 1.5)]



        public void TestMethod1(double firsNumber, double secondNumber, double ex_result)
        {
            double result = new Calculate().DivisionFrom(firsNumber, secondNumber);
            Assert.AreEqual(ex_result, result);
        }
    }
}
